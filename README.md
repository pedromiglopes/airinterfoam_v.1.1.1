# airInterFoam_v.1.1.1 #
*multi-scale solver approach for dispersed fluids with VOF method*

________________________________________________________________

Copyright Information

    Copyright (C) 1991-2014 OpenCFD Ltd.
    Copyright (C) 2014-2016 Pedro Lopes

License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Target platform

    The code is known to work with OpenFOAM 2.3.x.

Author

    Pedro Lopes <pmlopes@student.dec.uc.pt>

________________________________________________________________

Instructions
------------

Download the source code using git:

    git clone git@bitbucket.org:pedromiglopes/airinterfoam_v.1.1.1.git

Enter the directory where the source code has been extracted, and compile it by typing:

    wmake


Disclaimer
----------
This offering is not approved or endorsed by OpenCFD Limited, the producer
of the OpenFOAM software and owner of the OPENFOAM®  and OpenCFD®  trade marks.

Detailed information on the OpenFOAM trademark can be found at

 - http://www.openfoam.com/legal/trademark-policy.php
 - http://www.openfoam.com/legal/trademark-guidelines.php

For further information on OpenCFD and OpenFOAM, please refer to

 - http://www.openfoam.com


Version track
----------
V1.1.1 - airInterFoam model
