/*---------------------------------------------------------------------------*\

                                airInterFoam

\*---------------------------------------------------------------------------*/

Info<< "Reading field alphag\n" << endl;
volScalarField alphag
(
    IOobject
    (
        "alphag",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

volScalarField Sg
(
    IOobject
    (
        "Sg",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("Sg",dimensionSet(0,-3,-1,0,0,0,0),0.0)
);
Sg.write();

volScalarField UnHat
(
    IOobject
    (
        "UnHat",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::NO_WRITE
    ),
    mesh,
    dimensionedScalar("UnHat",dimensionSet(0,1,-1,0,0,0,0),0.0)
);
UnHat.write();

volScalarField UgHat
(
    IOobject
    (
        "UgHat",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::NO_WRITE
    ),
    mesh,
    dimensionedScalar("UgHat",dimensionSet(0,1,-1,0,0,0,0),0.0)
);
UgHat.write();

volScalarField SgFrac
(
    IOobject
    (
        "SgFrac",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("SgFrac",dimensionSet(0,0,-1,0,0,0,0),0.0)
);
SgFrac.write();

volVectorField Ug
(
    IOobject
    (
        "Ug",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::AUTO_WRITE
    ),
    U
);
Ug.write();

volVectorField Ur
(
    IOobject
    (
        "Ur",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::NO_WRITE
    ),
    (Ug - U)
);
Ur.write();

// *** Variables stored for ddt and div

surfaceScalarField phiUg
(
    IOobject
    (
        "phiUg",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    linearInterpolate(Ug) & mesh.Sf()
);

volScalarField rhogAlphag
(
    IOobject
    (
        "rhogAlphag",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT
    ),
    rhog*(alphag+SMALL),
    alphag.boundaryField().types()
);

surfaceScalarField rhogAlphagPhi
(
    IOobject
    (
        "rhogAlphagPhi",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    fvc::interpolate(rhogAlphag) * linearInterpolate(Ug) & mesh.Sf()
);

// *** Other Variables

volScalarField l_turb
(
    IOobject
    (
        "l_turb",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("l_turb",dimensionSet(0,1,0,0,0,0,0),0.0)
);

volScalarField l_int
(
    IOobject
    (
        "l_int",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("l_int",dimensionSet(0,1,0,0,0,0,0),0.0)
);

volScalarField l_tHirt
(
    IOobject
    (
        "l_tHirt",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("l_tHirt",dimensionSet(0,1,0,0,0,0,0),0.0)
);

volVectorField gradAlpha1
(
    IOobject
    (
        "gradAlpha1",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    mesh,
    dimensionedVector("gradAlpha1",dimensionSet(0,-1,0,0,0,0,0),vector(0.0,0.0,0.0))
);

volVectorField nHat
(
IOobject
    (
        "nHat",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    mesh,
    dimensionedVector("nHat",dimensionSet(0,0,0,0,0,0,0),vector(0.0,0.0,0.0))
);

volScalarField dUdn
(
IOobject
   (
        "dUdn",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
   ),
   mesh,
   dimensionedScalar("dUdn",dimensionSet(0,0,-1,0,0,0,0),0.0)
);

volScalarField ws
(
IOobject
   (
        "ws",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
   ),
   mesh,
   dimensionedScalar("ws",dimensionSet(0,0,0,0,0,0,0),1.0)
);

volScalarField Re_b
(
    IOobject
    (
        "Re_b",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    mesh,
    dimensionedScalar("Re_b",dimensionSet(0,0,0,0,0,0,0),0.0)
);

volScalarField phiFS
(
    IOobject
    (
        "phiFS",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("phiFS",dimensionSet(0,0,0,0,0,0,0),0.0)
);

Info<< "Reading transportProperties\n" << endl;

IOdictionary transportProperties
(
    IOobject
    (
        "transportProperties",
        runTime.constant(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);

dictionary phase1 = transportProperties.subDict("water");
dimensionedScalar nu1 = phase1.lookup("nu");

dictionary phase2 = transportProperties.subDict("air");
dimensionedScalar nu2 = phase2.lookup("nu");

dimensionedScalar sigma = transportProperties.lookup("sigma");

Info << "End of createFields\n" << endl;
